const express = require('express')
const routetudu = require('./routes/tudu')

const app = express()
app.use(express.json())
require('./routes/tudu')(app)
const port = 3200

app.use(routetudu)


console.log(`your server was running on ${port}`)
app.listen(port)