'use strict';
module.exports = (sequelize, DataTypes) => {
  const tudu = sequelize.define('tudu', {
    title: DataTypes.STRING,
    description: DataTypes.STRING
  }, {});
  tudu.associate = function(models) {
    // associations can be defined here
  };
  return tudu;
};