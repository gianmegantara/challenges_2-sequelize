var model = require("../models/index");

module.exports = (app) => {
    /* GET tudu listing. */
    app.get("/tudus", function (req, res, next) {
        model.tudu.findAll({})
            .then(tudus =>
                res.json({
                    error: false,
                    data: tudus
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    data: [],
                    error: error
                })
            );
    });

    /* POST tudu. */
    app.post("/tudus", function (req, res, next) {
        const {
            title,
            description
        } = req.body;
        model.tudu.create({
                title: title,
                description: description
            })
            .then(tudu =>
                res.status(201).json({
                    error: false,
                    data: tudu,
                    message: "New tudu has been created."
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    data: [],
                    error: error
                })
            );
    });

    /* update tudu. */
    app.put("/tudus/:id", function (req, res, next) {
        const tudu_id = req.params.id;

        const {
            title,
            description
        } = req.body;

        model.tudu.update({
                title: title,
                description: description
            }, {
                where: {
                    id: tudu_id
                }
            })
            .then(tudu =>
                res.json({
                    error: false,
                    message: "tudu has been updated."
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    error: error
                })
            );
    });

    /* GET tudu listing. */
    /* Delete tudu. */
    app.delete("/tudus/:id", function (req, res, next) {
        const tudu_id = req.params.id;

        model.tudu.destroy({
                where: {
                    id: tudu_id
                }
            })
            .then(status =>
                res.json({
                    error: false,
                    message: "tudu has been delete."
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    error: error
                })
            );
    });
};